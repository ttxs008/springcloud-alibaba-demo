package org.t230;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableDiscoveryClient
@RefreshScope
public class UserServiceProvider {
    public static void main(String[] args) {
        SpringApplication.run(UserServiceProvider.class, args);
    }

    @SentinelResource(value = "helloUser", blockHandler = "helloBlockHandler")
    @GetMapping("/hello")
    public Result hello(String name) {
        return Result.success("Hello " + name + ", userName=" + userName + ", age=" + age);
    }

    public Result helloBlockHandler(String name, BlockException ex) {
        return Result.error("UserServiceProvider.hello()被限流了:"+ name);
    }

    @Value("${user.name}")
    private String userName;
    @Value("${user.age}")
    private int age;
}
