package org.t230;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.apache.dubbo.config.annotation.DubboService;

@DubboService
public class UserServiceImpl implements UserServiceDubboApi {
    @Override
    @SentinelResource(value = "userServiceHello", blockHandler = "blockHandler")
    public String hello(String name) {
        return "HELLO DUBBO " + name;
    }

    public String blockHandler(String name, BlockException ex)  {
        return "userServiceHello被限流了" + name;
    }
}
