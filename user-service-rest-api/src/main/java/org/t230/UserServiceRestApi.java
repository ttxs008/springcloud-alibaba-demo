package org.t230;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "user-service-provider", fallback = UserServiceRestApiFallback.class)
public interface UserServiceRestApi {
    @GetMapping("/hello")
    Result hello(@RequestParam(value = "name") String name);
}
