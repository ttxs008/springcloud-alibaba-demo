package org.t230;

import org.springframework.stereotype.Component;

@Component
public class UserServiceRestApiFallback implements UserServiceRestApi {
    @Override
    public Result hello(String name) {
        return Result.error("UserServiceRestApi.hello()被降级了----"+ name);
    }
}
