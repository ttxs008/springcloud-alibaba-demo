package org.t230;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@SpringBootApplication
//@EnableDiscoveryClient
@RestController
@EnableFeignClients()
public class UserServiceConsumer {
    @Resource
    private UserServiceRestApi userServiceRestApi;

    @DubboReference(timeout = 1000, mock = "org.t230.UserServiceDubboApiMock")
    private UserServiceDubboApi userServiceDubboApi;

    public static void main(String[] args) {
        SpringApplication.run(UserServiceConsumer.class, args);
    }

    @GetMapping("/getHello")
    public Result getHello(String name) {
        return userServiceRestApi.hello(name);
    }

    @GetMapping("/getDubbo")
    public Result getDubbo(String name) {
        return Result.success(userServiceDubboApi.hello(name));
    }
}
